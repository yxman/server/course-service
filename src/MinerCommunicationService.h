//
// Created by user on 2019-11-02.
//

#ifndef USER_SERVICE_AND_OAUTH_MINORCOMMUNICATIONSERVICE_H
#define USER_SERVICE_AND_OAUTH_MINORCOMMUNICATIONSERVICE_H

#include <string>
#include <vector>
#include <yxlib.h>
#include <zmq.hpp>

constexpr auto REQUEST_TIMEOUT_MS = 2000; // -1 for no timeout
constexpr auto REPLY_TIMEOUT_MS = 10000;  // -1 for no timeout

class MinerCommunicationService {
public:
  void initializeConnections();
  void closeConnections();
  void sendTransaction(const std::string &transaction);
  std::string sendInfoRequest(MessageType type, const std::string &message);

private:
  std::shared_ptr<zmq::context_t> _context;
  std::shared_ptr<zmq::socket_t> _transactionConnection;
  std::shared_ptr<zmq::socket_t> _informationConnection;
};

#endif // USER_SERVICE_AND_OAUTH_MINORCOMMUNICATIONSERVICE_H
