#include "CourseRequestHandler.h"
#include "MinerCommunicationService.h"
#include "Proxy.h"
#include <cstdlib>
#include <thread>
#include <yxlib.h>

using namespace yx;

constexpr auto configFile = "/etc/yx/course/config.yaml";
constexpr auto configFileProxy = "/etc/yx/proxy/config.yaml";

void launchProxy(Proxy proxy) { proxy.start(); }

int main() {
  // Add Config files
  ConfigHelper::addFile(configFile);
  ConfigHelper::addFile(configFileProxy);

  // Launch proxy for miners
  Proxy proxy;
  std::thread t(launchProxy, proxy);

  // Create Server
  RestServer server;

  MinerCommunicationService minerService;
  minerService.initializeConnections();
  CourseRequestHandler::use(&minerService);

  // Add Endpoints
  server.post("/transaction", CourseRequestHandler::sendTransaction);
  server.post("/info/cours", CourseRequestHandler::getCourse);
  server.post("/info/etudiant", CourseRequestHandler::getStudent);
  server.post("/fichier/notes", CourseRequestHandler::getGradeFile);
  server.get("/admin/chaine/{id: [0-9]}", CourseRequestHandler::getBlockChain);
  server.get("/admin/logs/{id: [0-9]}", CourseRequestHandler::getLogs);

  server.start();

  minerService.closeConnections();
  proxy.closeConnections();

  return EXIT_SUCCESS;
}
