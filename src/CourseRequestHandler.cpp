#include "CourseRequestHandler.h"
#include <regex>
#include <utility>
#include <yxlib/RestServer.h>
#include <yxlib/oauthHandler.h>

// constexpr std::regex test;
const std::string COURSE_ID_REGEX = "[A-Za-z]{3}[0-9]{4}[A-Za-z]?";
const std::string ONLY_LETTERS_REGEX = "[A-Za-z0-9 ]+";
const std::string TRIMESTER_REGEX = "[0-9]{4}[1-3]";
const std::string TRIMESTER_STRING_REGEX = "[0-9]{4}[1-3]|*";

const std::string GRADE_REGEX =
    R"(A\*|A|B\+|B|C\+|C|D\+|D|E|F|IP|IV|S|I|J|P|R|SE|X|Y|Z)";
const std::string STUDENT_ID_REGEX = "[0-9]*";

constexpr auto MAX_TRIES = 5;

MinerCommunicationService *CourseRequestHandler::_communicationService;

bool isValidField(const std::string &text, const std::regex &expression) {
  return std::regex_match(text, expression);
}

void CourseRequestHandler::use(
    MinerCommunicationService *communicationService) {
  _communicationService = communicationService;
}

void CourseRequestHandler::sendTransaction(const yx::Rest::Request &req,
                                           yx::Rest::Response &res) {

  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken)) {
    yx::cout() << userToken << " n'a pas les permissions requises"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }

  yx::Rest::Json body = req.body();
  yx::Rest::Json transactionJson;
  Transaction transaction;

  if (!pingMiners()) {
    res.send(restbed::NOT_FOUND,
             "La transaction n'a pas pu etre traitee");
    return;
  }

  try {
    if (body.contains(TRANSACTION_FIELD) && body.contains(PDF_FIELD)) {
      transactionJson = req.body().at(TRANSACTION_FIELD);
      transaction = jsonToTransaction(transactionJson);
    } else {
      throw InvalidJsonFormatException();
    }

    if (isValidField(transaction.id, std::regex(COURSE_ID_REGEX)) &&
        isValidField(transaction.name, std::regex(ONLY_LETTERS_REGEX)) &&
        isValidField(std::to_string(transaction.trimester),
                     std::regex(TRIMESTER_REGEX))) {

      for (Result result : transaction.results) {
        if (!(isValidField(result.lastName, std::regex(ONLY_LETTERS_REGEX)) &&
              isValidField(result.firstName, std::regex(ONLY_LETTERS_REGEX)) &&
              isValidField(result.id, std::regex(STUDENT_ID_REGEX)) &&
              isValidField(result.grade, std::regex(GRADE_REGEX)))) {
          res.returnStatus(restbed::BAD_REQUEST);
          return;
        }
      }
    } else {
      res.returnStatus(restbed::BAD_REQUEST);
      return;
    }

    auto section = yx::ConfigHelper::getSection("PDF");
    std::string pdfPath = section.getStr("path");

    const std::string fileName = pdfPath + std::string(transaction.id) + "-" +
                                 std::to_string(transaction.trimester);
    std::ofstream file(fileName, std::ios::out | std::ios::trunc);
    file << body[PDF_FIELD];
    _communicationService->sendTransaction(transactionJson.dump());
  } catch (InvalidJsonFormatException &ex) {
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  yx::Rest::Json courseRequest;
  courseRequest[TRIMESTER_FIELD] = transaction.trimester;
  courseRequest[COURSE_ID_FIELD] = transaction.id;
  sleep(1);
  std::string response= _communicationService->sendInfoRequest(MessageType::COURSE, courseRequest.dump());
  Transaction sentTransaction = jsonToTransaction(transactionJson);
  Transaction receivedTransaction =jsonToTransaction(yx::Rest::Json::parse(response)[0]);
  if(sentTransaction == receivedTransaction){
    res.sendJson(restbed::OK, req.body());
    return;
  }

  res.sendJson(restbed::NOT_FOUND, "La transaction n'a pas ete ajoutee");
}

void CourseRequestHandler::getStudent(const yx::Rest::Request &req,
                                      yx::Rest::Response &res) {
  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken, 1)) {
    yx::cout() << userToken << " n'a pas les permissions requises"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }

  yx::Rest::Json studentReqJson = req.body();
  StudentRequest studentReq;

  try {
    studentReq = jsonToStudentRequest(studentReqJson);

    if (!(isValidField(studentReq.id, std::regex(COURSE_ID_REGEX)) &&
          isValidField(studentReq.studentId, std::regex(STUDENT_ID_REGEX)) //&&
          )) {
      res.returnStatus(restbed::BAD_REQUEST);
      return;
    }
    yx::Rest::Json msgJson;
    if (studentReq.trimester == "*") {
      msgJson[TRIMESTER_FIELD] = -1;
    } else {
      msgJson[TRIMESTER_FIELD] = std::stoi(studentReq.trimester);
    }

    msgJson[COURSE_ID_FIELD] = studentReq.id;

    std::string transactionStr =
        _communicationService->sendInfoRequest(COURSE, msgJson.dump());
    if (!transactionStr.empty()) {
      yx::Rest::Json transactionsJson = yx::Rest::Json::parse(transactionStr);
      yx::Rest::Json reply;
      for (const auto &transactionJson : transactionsJson) {
        Transaction transaction = jsonToTransaction(transactionJson);
        for (auto result : transaction.results) {
          if (result.id == studentReq.studentId) {

            yx::Rest::Json note;
            note[TRIMESTER_FIELD] = transaction.trimester;
            note[GRADE_FIELD] = result.grade;

            reply["notes"].push_back(note);
            reply[NAME_FIELD] = result.lastName;
            reply[FIRST_NAME_FIELD] = result.firstName;
          }
        }
      }
      if (!reply.empty()) {
        res.sendJson(restbed::OK, reply);
        return;
      }
    }
    res.returnStatus(restbed::NOT_FOUND);
    return;
  } catch (InvalidJsonFormatException &ex) {
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  res.returnStatus(restbed::OK);
}

void CourseRequestHandler::getCourse(const yx::Rest::Request &req,
                                     yx::Rest::Response &res) {
  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken,1)) {
    yx::cout() << userToken << " n'a pas les permissions requises"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }

  yx::Rest::Json courseReqJson = req.body();
  CourseRequest courseReq;

  try {
    courseReq = jsonToCourseRequest(courseReqJson);
    if (!(isValidField(courseReq.id, std::regex(COURSE_ID_REGEX)) &&
          isValidField(std::to_string(courseReq.trimester),
                       std::regex(TRIMESTER_REGEX)))) {
      res.returnStatus(restbed::BAD_REQUEST);
      return;
    }
    std::string transactionStr =
        _communicationService->sendInfoRequest(COURSE, courseReqJson.dump());
    if (!transactionStr.empty()) {
      yx::Rest::Json transactionJson = yx::Rest::Json::parse(transactionStr);
      if (!transactionJson.empty()) {
        transactionJson[0].erase(TRIMESTER_FIELD);
        transactionJson[0].erase(COURSE_ID_FIELD);
        res.sendJson(restbed::OK, transactionJson[0]);
        return;
      }
    }
    res.returnStatus(restbed::NOT_FOUND);
  } catch (InvalidJsonFormatException &ex) {

    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  res.returnStatus(restbed::OK);
}

void CourseRequestHandler::getGradeFile(const yx::Rest::Request &req,
                                        yx::Rest::Response &res) {
  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken,1)) {
    yx::cout() << userToken << " n'a pas les permissions requises"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }
  yx::Rest::Json fileRequest = req.body();

  if (!(fileRequest.contains(COURSE_ID_FIELD) &&
        fileRequest.contains(TRIMESTER_FIELD) &&
        isValidField(fileRequest[COURSE_ID_FIELD],
                     std::regex(COURSE_ID_REGEX)) &&
        isValidField(std::to_string(fileRequest[TRIMESTER_FIELD].get<int>()),
                     std::regex(TRIMESTER_REGEX)))) {

    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }
  std::string courseId = fileRequest[COURSE_ID_FIELD];

  auto section = yx::ConfigHelper::getSection("PDF");
  std::string pdfPath = section.getStr("path");
  std::string fileName =
      pdfPath + courseId + "-" +
      std::to_string(fileRequest[TRIMESTER_FIELD].get<int>());
  std::string base64;
  std::ifstream inFile(fileName);

  if (!inFile.is_open()) {
    yx::cerr() << "Le fichier " << fileName << " ne peut être lue."
               << std::endl;
    res.returnStatus(restbed::NOT_FOUND);
    return;
  }

  while (!inFile.eof()) {
    std::getline(inFile, base64);
  }

  yx::Rest::Json newBody;

  newBody["pdf"] = base64;

  res.sendJson(restbed::OK, newBody);
}

void CourseRequestHandler::getBlockChain(const yx::Rest::Request &req,
                                         yx::Rest::Response &res) {
  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken,2)) {
    yx::cout() << userToken << " n'a pas les permissions requises"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }
  yx::Rest::Json blockchainRequest = req.body();

  if (!blockchainRequest.contains("derniers_blocs")) {
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }

  size_t askedMinerId = std::stoi(req.getParam("id"));
  size_t minerId;
  std::string blockchain;
  size_t tries = 0;
  do {
    std::string receivedMessage = _communicationService->sendInfoRequest(
        BLOCKCHAIN, blockchainRequest.dump());

    if (receivedMessage.empty()) {
      res.send(restbed::NOT_FOUND, "Aucune reponse de mineur");
      return;
    }

    auto receivedJson = yx::Rest::Json::parse(receivedMessage);

    if (!(receivedJson.contains("id") && receivedJson.contains("blockchain"))) {
      yx::cwar() << " Le mineur a envoyer une reponse dans le mauvais format"
                 << std::endl;
      res.send(restbed::BAD_REQUEST, "Mauvais format recu");
      return;
    }

    minerId = receivedJson["id"];
    blockchain = receivedJson["blockchain"];

  } while (minerId != askedMinerId && ++tries < MAX_TRIES);

  if (minerId == askedMinerId) {
    yx::Rest::Json response;
    response["blocks"] = yx::Rest::Json::parse(blockchain);
    res.sendJson(200, response);
  } else {
    res.send(restbed::NOT_FOUND, "Ce mineur n'est pas accessible");
  }
}

void CourseRequestHandler::getLogs(const yx::Rest::Request &req,
                                   yx::Rest::Response &res) {
  const std::string userToken = req.accessToken();

  if (!OauthHandler::isvalidToken(userToken, 2)) {
    yx::cout() << userToken << " n'a pas les permissions requises"
               << std::endl;
    res.send(restbed::FORBIDDEN, "You need Admin access");
    return;
  }
  yx::Rest::Json logRequest = req.body();

  if (!logRequest.contains("dernier")) {
    res.returnStatus(restbed::BAD_REQUEST);
    return;
  }
  size_t askedMinerId = std::stoi(req.getParam("id"));
  size_t minerId;
  yx::Rest::Json logs;
  size_t tries = 0;
  do {
    std::string receivedMessage =
        _communicationService->sendInfoRequest(LOGS, logRequest.dump());

    if (receivedMessage.empty()) {
      res.send(restbed::NOT_FOUND, "Aucune reponse de mineur");
      return;
    }

    auto receivedJson = yx::Rest::Json::parse(receivedMessage);

    if (!(receivedJson.contains("id") && receivedJson.contains("logs"))) {
      yx::cwar() << " Le mineur a envoyer une reponse dans le mauvais format"
                 << std::endl;
      res.send(restbed::BAD_REQUEST, "Mauvais format recu");
      return;
    }

    minerId = receivedJson["id"];
    logs = receivedJson["logs"];

  } while (minerId != askedMinerId && ++tries < MAX_TRIES);

  if (minerId == askedMinerId) {
    res.sendJson(200, logs);
  } else {
    res.send(restbed::NOT_FOUND, "Ce mineur n'est pas accessible");
  }
}

bool CourseRequestHandler::pingMiners() {
  std::string res = _communicationService->sendInfoRequest(PING, "");
  return !res.empty();
}
