#include "MinerCommunicationService.h"
#include <string>
#include <yxlib/CommHelper.h>

enum PORT { P_TRANSACTION, P_INFO };

void MinerCommunicationService::initializeConnections() {
  // Get configurations
  yx::ConfigSection config = yx::ConfigHelper::getSection("zmq");

  std::string connectionPrefix = "tcp://";

  std::string ip = config.getStr("ip");
  std::vector<std::string> ports = config.getStrArr("ports");

  // Find the addresses
  std::string addrTransaction =
      connectionPrefix + ip + ":" + ports[P_TRANSACTION];
  std::string addrInfo = connectionPrefix + ip + ":" + ports[P_INFO];

  _context = std::make_shared<zmq::context_t>(1);

  _transactionConnection = yx::CommHelper::initConnection(
      _context, addrTransaction, zmq::socket_type::pub, true);

  std::vector<yx::CommHelper::Option> options;
  options.push_back({ZMQ_SNDTIMEO, REQUEST_TIMEOUT_MS});
  options.push_back({ZMQ_RCVTIMEO, REPLY_TIMEOUT_MS});
  _informationConnection = yx::CommHelper::initConnection(
      _context, addrInfo, zmq::socket_type::req, true, options);
}

void MinerCommunicationService::closeConnections() {
  _transactionConnection->close();
  _informationConnection->close();
}

void MinerCommunicationService::sendTransaction(
    const std::string &transaction) {
  yx::cout() << "Envoi d'une transaction..." << std::endl;
  yx::CommHelper::sendString(_transactionConnection, transaction);
}

std::string
MinerCommunicationService::sendInfoRequest(MessageType type,
                                           const std::string &message) {
  yx::cout() << "Envoi d'une requete d'information de type: " +
                    std::to_string(type) + " ..."
             << std::endl;
  bool isSentSuccess =
      yx::CommHelper::sendRequest(_informationConnection, type, message);
  if (isSentSuccess) {
    return yx::CommHelper::receiveString(_informationConnection);
  } else {
    yx::cwar() << "Aucun mineur ne repond" << std::endl;
  }

  return "";
}
