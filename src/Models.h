//
// Created by user on 2019-11-02.
//

#ifndef USER_SERVICE_AND_OAUTH_COURSE_H
#define USER_SERVICE_AND_OAUTH_COURSE_H

#include <string>
#include <vector>

// todo: check if it should go in yxlib

struct Result {
  std::string lastName;
  std::string firstName;
  std::string id;
  std::string mark;
};

struct Transaction {
  std::string id;
  std::string name;
  int trimester;
  std::vector<Result> results;
};

struct Course {
  std::string id;
  int trimester;
  std::vector<Result> results;
};

#endif // USER_SERVICE_AND_OAUTH_COURSE_H
