
#ifndef COURSE_SERVICE_PROXY_H
#define COURSE_SERVICE_PROXY_H

#include <memory>
#include <yxlib.h>

class Proxy {

public:
  Proxy();
  void closeConnections();
  void start();

private:
  void loadCache();
  void saveCache();
  void init();

private:
  std::shared_ptr<zmq::socket_t> _minerPub;
  std::shared_ptr<zmq::socket_t> _minerSub;
  std::shared_ptr<zmq::socket_t> _minerRep;
  std::shared_ptr<zmq::context_t> _context;
  zmq::pollitem_t _poller[2];
  std::vector<std::string> _cache;
  size_t _cacheIdx;
};

#endif // COURSE_SERVICE_PROXY_H
