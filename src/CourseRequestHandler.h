//
// Created by user on 2019-11-02.
//

#ifndef USER_SERVICE_AND_OAUTH_COURSEREQUESTHANDLER_H
#define USER_SERVICE_AND_OAUTH_COURSEREQUESTHANDLER_H

#include "../lib/yxlib/include/yxlib.h"
#include "MinerCommunicationService.h"

class CourseRequestHandler {
public:
  static void use(MinerCommunicationService *communicationService);
  static void sendTransaction(const yx::Rest::Request &req,
                              yx::Rest::Response &res);
  static void getCourse(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void getStudent(const yx::Rest::Request &req, yx::Rest::Response &res);
  static void getGradeFile(const yx::Rest::Request &req,
                           yx::Rest::Response &res);
  static void getBlockChain(const yx::Rest::Request &req,
                            yx::Rest::Response &res);
  static void getLogs(const yx::Rest::Request &req, yx::Rest::Response &res);

private:
  static bool pingMiners();
  static MinerCommunicationService *_communicationService;
};

#endif // USER_SERVICE_AND_OAUTH_COURSEREQUESTHANDLER_H
