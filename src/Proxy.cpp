//
// Created by immaybemarc on 11/23/19.
//

#include "Proxy.h"

using namespace yx;

constexpr auto CACHE_FIELD = "Cache";
constexpr auto CURRENT_IDX = "Current-Idx";
constexpr auto CACHE_FILE = "cache.json";

Proxy::Proxy() { init(); }

void Proxy::init() {
  _cacheIdx = 0;
  _context = std::make_unique<zmq::context_t>(1);

  auto config = ConfigHelper::getSection("miner");
  std::string ip = config.getStr("ip");
  auto ports = config.getConfig("ports");

  std::string pubPort = ports.getStr("publish");
  _minerPub = CommHelper::initConnection(
      _context, "tcp://" + ip + ':' + pubPort, zmq::socket_type::pub, true);

  std::string subPort = ports.getStr("subscribe");
  _minerSub = CommHelper::initConnection(
      _context, "tcp://" + ip + ':' + subPort, zmq::socket_type::sub, true);

  std::string repPort = ports.getStr("reply");
  _minerRep = CommHelper::initConnection(
      _context, "tcp://" + ip + ':' + repPort, zmq::socket_type::rep, true);

  _poller[0].socket = *_minerSub;
  _poller[0].events = ZMQ_POLLIN;
  _poller[1].socket = *_minerRep;
  _poller[1].events = ZMQ_POLLIN;
}

void Proxy::start() {

  bool isRunning = true;
  loadCache();
  while (isRunning) {
    int rec = zmq::poll(_poller, 2);
    std::string message;
    if (rec > 0) {
      message = CommHelper::receiveString(_minerSub, zmq::recv_flags::dontwait);
      if (!message.empty()) {
        _cache.push_back(message);
        saveCache();
        CommHelper::sendString(_minerPub, message);
      }

      message = CommHelper::receiveString(_minerRep, zmq::recv_flags::dontwait);
      if (!message.empty()) {
        int messageId = std::stoi(message);
        Rest::Json response;
        response["messages"] = Rest::Json::array();
        if (messageId - _cacheIdx >= 0) {
          for (size_t i = messageId - _cacheIdx; i < _cache.size(); ++i) {
            response["messages"].push_back(_cache[i]);
          }
          CommHelper::sendString(_minerRep, response.dump());
        }
      }
    }
  }
}
void Proxy::closeConnections() {
  _minerPub->close();
  _minerRep->close();
  _minerSub->close();
}

void Proxy::loadCache() {
  auto section = yx::ConfigHelper::getSection("cache");
  std::string cachePath = section.getStr("path");
  _cache = std::vector<std::string>();
  std::ifstream inFile(cachePath + CACHE_FILE);
  if (inFile.good()) {
    std::stringstream strStream;
    strStream << inFile.rdbuf(); //read the file
    std::string cacheStr = strStream.str(); //str holds the content of the file

    try { // Assume the local json contains a valid blockchain
      auto jsonData = yx::Rest::Json::parse(cacheStr);
      _cacheIdx = jsonData[CURRENT_IDX];
      yx::Rest::Json cacheList = jsonData[CACHE_FIELD];
      for(const auto& cacheItem:cacheList){
        _cache.push_back(cacheItem);
      }
    }
    catch (...){
      yx::cerr() << "La cache n'a pas pu etre charger" << std::endl;
      _cacheIdx = 0;
      _cache = std::vector<std::string>();
    }
  }
}

void Proxy::saveCache() {
  auto section = yx::ConfigHelper::getSection("cache");
  std::string cachePath = section.getStr("path");

  yx::Rest::Json jsonData;
  jsonData[CURRENT_IDX] = _cacheIdx;
  for(const auto& cacheItem:_cache){
    jsonData[CACHE_FIELD].push_back(cacheItem);
  }

  std::ofstream out(cachePath + CACHE_FILE, std::ios::out);
  out << jsonData.dump(1);
  out.close();
}
